package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitee.com/itsos/wxsdk/wxchat"
)

func main() {
	http.HandleFunc("/jssdk", func(writer http.ResponseWriter, request *http.Request) {
		values := request.URL.Query()
		res := wxchat.NewJsapiTicket().GetJsSdk(values.Get("url"))
		jssdk, err := json.Marshal(res)
		if err != nil {
			log.Fatal(err)
		}
		writer.Write(jssdk)
	})

	http.Handle("/", http.FileServer(http.Dir("./static")))

	port := "8080"
	log.Printf(fmt.Sprintf("About to listen on %s. Go to http://localhost:%s/", port, port))
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
