/*
   Copyright (c) [2021] IT.SOS
   wxsdk is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2.
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
   See the Mulan PSL v2 for more details.
*/

package wxchat

import "gitee.com/itsos/wxsdk/common/global"

// url
const (
	// 网页授权
	getSnsCodeUrl         = global.BaseApiUrl + "/connect/oauth2/authorize?appid={appid}&redirect_uri={redirect_uri}&response_type=code&scope={scope}&state={state}#wechat_redirect"
	getSnsAccessTokenUrl  = global.BaseOpenUrl + "/sns/oauth2/access_token?appid={appid}&secret={secret}&code={code}&grant_type=authorization_code"
	getSnsRefreshTokenUrl = global.BaseApiUrl + "/sns/oauth2/refresh_token?appid={appid}&grant_type=refresh_token&refresh_token={refresh_token}"
	getSnsUserInfoUrl     = global.BaseOpenUrl + "/sns/userinfo?access_token={access_token}&openid={openid}&lang=zh_CN"
	authSnsAccessTokenUrl = global.BaseApiUrl + "/sns/auth?access_token={access_token}&openid={openid}"

	// 全局access_token
	getAccessToken = global.BaseApiUrl + "/cgi-bin/token?grant_type=client_credential&appid={appid}&secret={secret}"

	// 全局jsapi_ticket
	getJsapiTicket = global.BaseApiUrl + "/cgi-bin/ticket/getticket?access_token={access_token}&type=jsapi"
)
