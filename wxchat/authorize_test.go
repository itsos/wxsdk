/*
   Copyright (c) [2021] IT.SOS
   wxsdk is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2.
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
   See the Mulan PSL v2 for more details.
*/

package wxchat

import (
	"testing"
)

func Test_authorize_SnsAccessToken(t *testing.T) {
}

func Test_authorize_SnsApiBase(t *testing.T) {
	t.Log(NewAuthorize().SnsApiBase("http://redirectUri", "state"))
}

func Test_authorize_SnsApiUserInfo(t *testing.T) {
}

func Test_authorize_SnsAuthAccessToken(t *testing.T) {
}

func Test_authorize_SnsRefreshToken(t *testing.T) {
}

func Test_authorize_SnsUserInfo(t *testing.T) {
}
