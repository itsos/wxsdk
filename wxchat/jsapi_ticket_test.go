package wxchat

import (
	"testing"
	"time"
)

func Test_jsapiTicket_getJsapiTicket(t *testing.T) {
	t.Log(NewJsapiTicket().getJsapiTicket())
}

func BenchmarkJsapiTicket_getJsapiTicket(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			b.Log(NewJsapiTicket().getJsapiTicket())
			time.Sleep(time.Millisecond * 400)
		}
	})
}

func TestJsapiTicket_GetJsSdk(t *testing.T) {
	t.Log(NewJsapiTicket().GetJsSdk("http://demo/jssdk/"))
}
