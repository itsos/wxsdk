package wxchat

import (
	"math/rand"
	"testing"
	"time"
)

type tests struct {
	a int32
}

func BenchmarkAccessToken_struct(b *testing.B) {
	//go func() {
	//	c := &chatHttp{}
	//	c.setExpire(1)
	//	time.Sleep(time.Millisecond * 200)
	//	println(c.getExpire())
	//}()

	//b.SetParallelism(4)
	var t = tests{}
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			num := rand.Int31n(1000000)
			t.a = num
			time.Sleep(time.Millisecond * 200)
			if t.a != num {
				b.Error("Variable override")
			}
		}
	})
}
