package wxchat

import (
	"testing"
	"time"
)

func Test_accessToken_GetAccessToken(t *testing.T) {
	t.Log(NewAccessToken().getAccessToken())
}

func BenchmarkAccessToken_GetAccessToken(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			b.Log(NewAccessToken().getAccessToken())
			time.Sleep(time.Millisecond * 400)
		}
	})
}
