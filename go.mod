module gitee.com/itsos/wxsdk

go 1.15

require (
	github.com/it-sos/golibs v1.0.2
	github.com/spf13/viper v1.13.0
)
