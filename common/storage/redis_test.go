/*
   Copyright (c) [2021] IT.SOS
   wxsdk is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2.
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
   See the Mulan PSL v2 for more details.
*/

package storage

import (
	"testing"
	"time"
)

const redisKey = "test"

func Test_redis_Get(t *testing.T) {
	t.Log(storeRedis().Get(redisKey))
}

func Test_redis_GetExpire(t *testing.T) {
	ttl, err := storeRedis().GetExpire(redisKey)
	t.Log(err)
	t.Log(ttl.Seconds() < 0)
}

func Test_redis_Lock(t *testing.T) {
	t.Log(storeRedis().Lock(redisKey, time.Minute))
}

func Test_redis_Set(t *testing.T) {
	//t.Log(storeRedis().Set(key, "1", time.Minute*2))
	Store().Set("test", string("{}"), time.Duration(time.Second*7000))
}
