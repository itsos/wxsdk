/*
   Copyright (c) [2021] IT.SOS
   wxsdk is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2.
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
   See the Mulan PSL v2 for more details.
*/

package storage

import (
	"testing"
	"time"
)

const redisClusterKey = "test-cluster"

func Test_redis_cluster_Get(t *testing.T) {
	t.Log(storeRedisCluster().Get(redisClusterKey))
}

func Test_redis_cluster_GetExpire(t *testing.T) {
	ttl, err := storeRedisCluster().GetExpire(redisClusterKey)
	t.Log(err)
	t.Log(ttl.Seconds() < 0)
}

func Test_redis_cluster_Lock(t *testing.T) {
	t.Log(storeRedisCluster().Lock(redisClusterKey, time.Minute))
}

func Test_redis_cluster_Set(t *testing.T) {
	//t.Log(storeRedisCluster().Set(key, "1", time.Minute*2))
	Store().Set(redisClusterKey, string("{}"), time.Duration(time.Second*7000))
}
