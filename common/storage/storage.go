package storage

import (
	"gitee.com/itsos/wxsdk/common/config"
	"time"
)

const (
	storageRedis        = "redis"
	storageRedisCluster = "redis-cluster"
)

type Storage interface {
	Set(key, value string, duration time.Duration) error
	Get(key string) (string, error)
	GetExpire(key string) (time.Duration, error)
	Remove(key string) error
	Lock(key string, duration time.Duration) (bool, error)
}

func Store() Storage {
	storage := config.Cfg.GetWxStorage()
	if storage == storageRedis {
		return storeRedis()
	} else if storage == storageRedisCluster {
		return storeRedisCluster()
	} else {
		panic("配置 wx.storage 配置不正确，选择: [redis | redis-cluster]：" + config.Cfg.GetWxStorage())
	}
	return nil
}
