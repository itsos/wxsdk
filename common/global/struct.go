package global

type ErrorDTO struct {
	ErrCode int    `json:"errcode"`
	ErrMsg  string `json:"errmsg"`
}
