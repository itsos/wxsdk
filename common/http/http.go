package http

import (
	"io/ioutil"
	"net/http"
)

// Get 发起get请求
func Get(url string) []byte {
	resp, err := http.Get(url)
	if err != nil {
		panic("http get fail." + err.Error())
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic("read body fail." + err.Error())
	}
	return body
}
