/*
   Copyright (c) [2021] itsos
   golibs is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2.
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
   See the Mulan PSL v2 for more details.
*/

package config

import (
	"reflect"

	"github.com/it-sos/golibs/config"
	"github.com/it-sos/golibs/utils/reflects"
	"github.com/spf13/viper"
)

// web 应用基础配置

type ConfigurationReadOnly interface {
	config.ConfigurationReadOnly
	GetWxAppId() string
	GetWxAppSecret() string
	GetWxToken() string
	GetWxEncodingAesKey() string
	GetWxStorage() string
	GetWxDebug() bool
}

type Configuration struct {
	*config.Configuration
	WxAppId          string `yaml:"wx.appid"`
	WxAppSecret      string `yaml:"wx.appsecret"`
	WxToken          string `yaml:"wx.token"`
	WxEncodingAesKey string `yaml:"wx.encoding_aes_key"`
	WxStorage        string `yaml:"wx.storage"`
	WxDebug          string `yaml:"wx.debug"`
}

func (c Configuration) GetWxAppId() string {
	return viper.GetString(c.WxAppId)
}

func (c Configuration) GetWxAppSecret() string {
	return viper.GetString(c.WxAppSecret)
}

func (c Configuration) GetWxToken() string {
	return viper.GetString(c.WxToken)
}

func (c Configuration) GetWxEncodingAesKey() string {
	return viper.GetString(c.WxEncodingAesKey)
}

func (c Configuration) GetWxStorage() string {
	return viper.GetString(c.WxStorage)
}

func (c Configuration) GetWxDebug() bool {
	return viper.GetBool(c.WxDebug)
}

var Cfg ConfigurationReadOnly = covertConfiguration()

func covertConfiguration() *Configuration {
	c := &Configuration{
		Configuration: config.Config,
	}
	t := reflect.ValueOf(c).Elem()
	reflects.TagToValueFlip(t, reflects.YAML)
	return c
}
